<?php

namespace Sitec\Laracogs;

use Illuminate\Support\ServiceProvider;
use Yab\Cerebrum\CerebrumProvider;
use Yab\CrudMaker\CrudMakerProvider;
use Yab\Crypto\CryptoProvider;
use Yab\FormMaker\FormMakerProvider;
use Sitec\Laracogs\Console\Activity;
use Sitec\Laracogs\Console\Api;
use Sitec\Laracogs\Console\Billing;
use Sitec\Laracogs\Console\Bootstrap;
use Sitec\Laracogs\Console\Docs;
use Sitec\Laracogs\Console\Features;
use Sitec\Laracogs\Console\Notifications;
use Sitec\Laracogs\Console\Semantic;
use Sitec\Laracogs\Console\Socialite;
use Sitec\Laracogs\Console\Starter;
use Yab\LaraTest\LaraTestProvider;

class LaracogsProvider extends ServiceProvider
{
    /**
     * Boot method.
     */
    public function boot()
    {
        // do nothing
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        /*
        |--------------------------------------------------------------------------
        | Providers
        |--------------------------------------------------------------------------
        */

        $this->app->register(FormMakerProvider::class);
        $this->app->register(CryptoProvider::class);
        $this->app->register(CrudMakerProvider::class);
        $this->app->register(CerebrumProvider::class);
        $this->app->register(LaraTestProvider::class);

        /*
        |--------------------------------------------------------------------------
        | Register the Commands
        |--------------------------------------------------------------------------
        */

        $this->commands([
            Activity::class,
            Api::class,
            Billing::class,
            Notifications::class,
            Features::class,
            Socialite::class,
            Bootstrap::class,
            Semantic::class,
            Docs::class,
            Starter::class,
        ]);
    }
}
